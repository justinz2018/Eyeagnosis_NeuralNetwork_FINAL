from sklearn import *
from sklearn.metrics import *
import matplotlib.pyplot as plt

f = open("plots_resnet_768/final_eval_pred.txt").read()
f2 = open("plots_resnet_768/final_eval_true.txt").read()

f = eval(f)
f2 = eval(f2)

fpr, tpr, _ = roc_curve(f2, f)
f_auc = auc(fpr, tpr)

plt.figure()
lw = 2
plt.plot(fpr, tpr, color='darkorange',
         lw=lw, label='ROC curve (area = %0.3f)' % f_auc)
print(f_auc)
plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver operating characteristic curve')
plt.legend(loc="lower right")
plt.show()
