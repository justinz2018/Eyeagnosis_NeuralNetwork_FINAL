# Resnet-50 model (up to line 143) from https://github.com/fchollet/keras/blob/master/examples/resnet_50.py
''' Code up to line 143 is licensed as follows: COPYRIGHT  All contributions by François Chollet: Copyright (c) 2015, François Chollet. All rights reserved.  All contributions by Google: Copyright (c) 2015, Google, Inc. All rights reserved.  All other contributions: Copyright (c) 2015, the respective contributors. All rights reserved.  Each contributor holds copyright over their respective contributions. The project versioning (Git) records all such contribution source information.  LICENSE  The MIT License (MIT)  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.'''
# Dropout and l2 regularization were added
from __future__ import print_function
import random, glob, sys

from keras.layers import merge
from keras.layers.convolutional import Convolution2D, MaxPooling2D, ZeroPadding2D, AveragePooling2D
from keras.layers.core import Dense, Activation, Flatten, Dropout
from keras.layers.normalization import BatchNormalization
from keras.models import Model
from keras.layers import Input
from keras.optimizers import Adam
from keras.preprocessing.image import load_img, img_to_array
import keras.backend as K
from keras.metrics import binary_crossentropy
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import EarlyStopping, ModelCheckpoint, History
from keras.regularizers import l2
import numpy as np
import matplotlib.pyplot as plt


def identity_block(input_tensor, kernel_size, filters, stage, block):
    dim_ordering = K.image_dim_ordering()
    nb_filter1, nb_filter2, nb_filter3 = filters
    if dim_ordering == 'tf':
        bn_axis = 3
    else:
        bn_axis = 1
    conv_name_base = 'res' + str(stage) + block + '_branch'
    bn_name_base = 'bn' + str(stage) + block + '_branch'

    out = Convolution2D(nb_filter1, 1, 1, dim_ordering=dim_ordering, name=conv_name_base + '2a', W_regularizer=l2(W_REG))(input_tensor)
    out = BatchNormalization(axis=bn_axis, name=bn_name_base + '2a')(out)
    out = Activation('relu')(out)
    if DROPOUT is not None:
        out = Dropout(DROPOUT)(out)

    out = Convolution2D(nb_filter2, kernel_size, kernel_size, border_mode='same',
                        dim_ordering=dim_ordering, name=conv_name_base + '2b', W_regularizer=l2(W_REG))(out)
    out = BatchNormalization(axis=bn_axis, name=bn_name_base + '2b')(out)
    out = Activation('relu')(out)
    if DROPOUT is not None:
        out = Dropout(DROPOUT)(out)

    out = Convolution2D(nb_filter3, 1, 1, dim_ordering=dim_ordering, name=conv_name_base + '2c', W_regularizer=l2(W_REG))(out)
    out = BatchNormalization(axis=bn_axis, name=bn_name_base + '2c')(out)
    if DROPOUT is not None:
        out = Dropout(DROPOUT)(out)

    out = merge([out, input_tensor], mode='sum')
    if DROPOUT is not None:
        out = Dropout(DROPOUT)(out)
    out = Activation('relu')(out)
    return out


def conv_block(input_tensor, kernel_size, filters, stage, block, strides=(2, 2)):
    nb_filter1, nb_filter2, nb_filter3 = filters
    dim_ordering = K.image_dim_ordering()
    if dim_ordering == 'tf':
        bn_axis = 3
    else:
        bn_axis = 1
    conv_name_base = 'res' + str(stage) + block + '_branch'
    bn_name_base = 'bn' + str(stage) + block + '_branch'

    out = Convolution2D(nb_filter1, 1, 1, subsample=strides,
                        dim_ordering=dim_ordering, name=conv_name_base + '2a', W_regularizer=l2(W_REG))(input_tensor)
    out = BatchNormalization(axis=bn_axis, name=bn_name_base + '2a')(out)
    out = Activation('relu')(out)
    if DROPOUT is not None:
        out = Dropout(DROPOUT)(out)

    out = Convolution2D(nb_filter2, kernel_size, kernel_size, border_mode='same',
                        dim_ordering=dim_ordering, name=conv_name_base + '2b', W_regularizer=l2(W_REG))(out)
    out = BatchNormalization(axis=bn_axis, name=bn_name_base + '2b')(out)
    out = Activation('relu')(out)
    if DROPOUT is not None:
        out = Dropout(DROPOUT)(out)

    out = Convolution2D(nb_filter3, 1, 1, dim_ordering=dim_ordering, name=conv_name_base + '2c', W_regularizer=l2(W_REG))(out)
    out = BatchNormalization(axis=bn_axis, name=bn_name_base + '2c')(out)
    if DROPOUT is not None:
        out = Dropout(DROPOUT)(out)

    shortcut = Convolution2D(nb_filter3, 1, 1, subsample=strides,
                             dim_ordering=dim_ordering, name=conv_name_base + '1', W_regularizer=l2(W_REG))(input_tensor)
    shortcut = BatchNormalization(axis=bn_axis, name=bn_name_base + '1')(shortcut)
    if DROPOUT is not None:
        shortcut = Dropout(DROPOUT)(shortcut)

    out = merge([out, shortcut], mode='sum')
    if DROPOUT is not None:
        out = Dropout(DROPOUT)(out)
    out = Activation('relu')(out)
    return out


def get_resnet50():
    if K.image_dim_ordering() == 'tf':
        inp = Input(shape=(224, 224, 3))
        bn_axis = 3
    else:
        inp = Input(shape=(3, 224, 224))
        bn_axis = 1

    dim_ordering = K.image_dim_ordering()
    out = ZeroPadding2D((3, 3), dim_ordering=dim_ordering)(inp)
    out = Convolution2D(64, 7, 7, subsample=(2, 2), dim_ordering=dim_ordering, name='conv1', W_regularizer=l2(W_REG))(out)
    out = BatchNormalization(axis=bn_axis, name='bn_conv1')(out)
    out = Activation('relu')(out)
    out = MaxPooling2D((3, 3), strides=(2, 2), dim_ordering=dim_ordering)(out)

    out = conv_block(out, 3, [64, 64, 256], stage=2, block='a', strides=(1, 1))
    out = identity_block(out, 3, [64, 64, 256], stage=2, block='b')
    out = identity_block(out, 3, [64, 64, 256], stage=2, block='c')

    out = conv_block(out, 3, [128, 128, 512], stage=3, block='a')
    out = identity_block(out, 3, [128, 128, 512], stage=3, block='b')
    out = identity_block(out, 3, [128, 128, 512], stage=3, block='c')
    out = identity_block(out, 3, [128, 128, 512], stage=3, block='d')

    out = conv_block(out, 3, [256, 256, 1024], stage=4, block='a')
    out = identity_block(out, 3, [256, 256, 1024], stage=4, block='b')
    out = identity_block(out, 3, [256, 256, 1024], stage=4, block='c')
    out = identity_block(out, 3, [256, 256, 1024], stage=4, block='d')
    out = identity_block(out, 3, [256, 256, 1024], stage=4, block='e')
    out = identity_block(out, 3, [256, 256, 1024], stage=4, block='f')

    out = conv_block(out, 3, [512, 512, 2048], stage=5, block='a')
    out = identity_block(out, 3, [512, 512, 2048], stage=5, block='b')
    out = identity_block(out, 3, [512, 512, 2048], stage=5, block='c')

    out = AveragePooling2D((7, 7), dim_ordering=dim_ordering)(out)
    out = Flatten()(out)
    out = Dense(1, activation='sigmoid', name='fc1000', W_regularizer=l2(W_REG))(out)

    model = Model(inp, out)

    return model

img_rows = img_cols = 224
train_data_dir = "train"
test_data_dir = "test"
validation_data_dir = "validation"
batch_size = 16
WEIGHTS_FN = "weights_resnet/{epoch:02d}-{val_loss:.2f}.hdf5"
RNG_SEED = 12345678
DROPOUT = .5
NB_EPOCH = 200 # rely on early stopping!
LR = .000033333
W_REG = .000000

af_ratio = len(glob.glob(train_data_dir+"/0/*.jpeg", recursive=True)) / len(glob.glob(train_data_dir+"/1/*.jpeg", recursive=True))
total_train = len(glob.glob(train_data_dir+"/**/*.jpeg", recursive=True))
total_val = len(glob.glob(validation_data_dir+"/**/*.jpeg", recursive=True))
total_test = len(glob.glob(test_data_dir+"/**/*.jpeg", recursive=True))

sys.setrecursionlimit(10000)
random.seed(RNG_SEED)
np.random.seed(random.randint(0, 4294967295))

def scaled_binary_crossentropy(y_true, y_pred):
	cost = binary_crossentropy(y_true, y_pred)
	mult = y_true*(af_ratio-1) + 1
	print(mult)
	return cost * mult
	
def f1_score(y_true, y_pred):
	y_pred = K.round(y_pred)
	tp = K.sum(K.equal(y_pred, 1) * K.equal(y_true, 1))
	fp = K.sum(K.equal(y_pred, 1) * K.equal(y_true, 0))
	fn = K.sum(K.equal(y_pred, 0) * K.equal(y_true, 1))
	precision = K.switch(K.equal(tp, 0), 0, tp/(tp+fp))
	recall = K.switch(K.equal(tp, 0), 0, tp/(tp+fn))
	return K.switch(K.equal(precision+recall, 0), 0, 2*(precision*recall)/(precision+recall))

if __name__ == '__main__':
	resnet_model = get_resnet50()
	resnet_model.compile(loss=scaled_binary_crossentropy,
              optimizer=Adam(lr=LR),
              metrics=["accuracy", f1_score])
    
		
	train_datagen = ImageDataGenerator(
			horizontal_flip=True,
			vertical_flip=True,
			rescale=1/255)
			
	test_datagen = ImageDataGenerator(rescale=1/255)

	train_generator = train_datagen.flow_from_directory(
			train_data_dir,
			target_size=(img_rows, img_cols),
			batch_size=batch_size,
			class_mode="binary",
			shuffle=True,
		   )
			
	validation_generator = test_datagen.flow_from_directory(
			validation_data_dir,
			target_size=(img_rows, img_cols),
			batch_size=batch_size,
			class_mode="binary",
		   )

	test_generator = test_datagen.flow_from_directory(
			test_data_dir,
			target_size=(img_rows, img_cols),
			batch_size=batch_size,
			class_mode="binary",
		 )
	try:
		history = History()
		resnet_model.fit_generator(
			train_generator,
			samples_per_epoch=total_train,
			nb_epoch=NB_EPOCH,
			validation_data=validation_generator,
			nb_val_samples=total_val,
			callbacks=[
				ModelCheckpoint(WEIGHTS_FN),
				history
				]
			)
	except KeyboardInterrupt:
		pass
	resnet_model.save_weights(WEIGHTS_FN)
	print(history.history)
	f = open("history.txt", "w")
	f.write(str(history.history))
	f.close()
	try:
		val_loss, = plt.plot(history.history["val_loss"], "r-", label="Validation Loss")
		loss, = plt.plot(history.history["loss"], "b-", label="Training Loss")
		plt.title("Loss scores (scaled binary crossentropy)")
		plt.legend(handles=[val_loss, loss])
		plt.savefig("loss_fig.png")
		plt.close()
		val_f1, = plt.plot(history.history["val_f1_score"], "r-", label="Validation F1")
		f1, = plt.plot(history.history["f1_score"], "b-", label="Test F1")
		plt.title("F1 scores")
		plt.legend(handles=[val_f1, f1])
		plt.savefig("f1_fig.png")
		plt.close()
	except:
		pass
	scores = resnet_model.evaluate_generator(test_generator, val_samples=total_test)
	print(scores)
