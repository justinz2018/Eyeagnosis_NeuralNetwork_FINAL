# Resnet-50 model (up to line 128) from https://github.com/fchollet/keras/blob/master/examples/resnet_50.py

from __future__ import print_function
import random, sys

from keras.layers import merge
from keras.layers.convolutional import Convolution2D, MaxPooling2D, ZeroPadding2D, AveragePooling2D
from keras.layers.core import Dense, Activation, Flatten, Dropout
from keras.layers.normalization import BatchNormalization
from keras.models import Model
from keras.layers import Input
from keras.optimizers import Adam
from keras.preprocessing.image import load_img, img_to_array
import keras.backend as K
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import EarlyStopping, ModelCheckpoint, History
from keras.regularizers import l2
import numpy as np
import matplotlib.pyplot as plt


def identity_block(input_tensor, kernel_size, filters, stage, block):
    dim_ordering = K.image_dim_ordering()
    nb_filter1, nb_filter2, nb_filter3 = filters
    if dim_ordering == 'tf':
        bn_axis = 3
    else:
        bn_axis = 1
    conv_name_base = 'res' + str(stage) + block + '_branch'
    bn_name_base = 'bn' + str(stage) + block + '_branch'

    out = Convolution2D(nb_filter1, 1, 1, dim_ordering=dim_ordering, name=conv_name_base + '2a', W_regularizer=l2(W_REG))(input_tensor)
    out = BatchNormalization(axis=bn_axis, name=bn_name_base + '2a')(out)
    out = Activation('relu')(out)
    if DROPOUT is not None:
        out = Dropout(DROPOUT)(out)

    out = Convolution2D(nb_filter2, kernel_size, kernel_size, border_mode='same',
                        dim_ordering=dim_ordering, name=conv_name_base + '2b', W_regularizer=l2(W_REG))(out)
    out = BatchNormalization(axis=bn_axis, name=bn_name_base + '2b')(out)
    out = Activation('relu')(out)
    if DROPOUT is not None:
        out = Dropout(DROPOUT)(out)

    out = Convolution2D(nb_filter3, 1, 1, dim_ordering=dim_ordering, name=conv_name_base + '2c', W_regularizer=l2(W_REG))(out)
    out = BatchNormalization(axis=bn_axis, name=bn_name_base + '2c')(out)
    if DROPOUT is not None:
        out = Dropout(DROPOUT)(out)

    out = merge([out, input_tensor], mode='sum')
    if DROPOUT is not None:
        out = Dropout(DROPOUT)(out)
    out = Activation('relu')(out)
    return out


def conv_block(input_tensor, kernel_size, filters, stage, block, strides=(2, 2)):
    nb_filter1, nb_filter2, nb_filter3 = filters
    dim_ordering = K.image_dim_ordering()
    if dim_ordering == 'tf':
        bn_axis = 3
    else:
        bn_axis = 1
    conv_name_base = 'res' + str(stage) + block + '_branch'
    bn_name_base = 'bn' + str(stage) + block + '_branch'

    out = Convolution2D(nb_filter1, 1, 1, subsample=strides,
                        dim_ordering=dim_ordering, name=conv_name_base + '2a', W_regularizer=l2(W_REG))(input_tensor)
    out = BatchNormalization(axis=bn_axis, name=bn_name_base + '2a')(out)
    out = Activation('relu')(out)
    if DROPOUT is not None:
        out = Dropout(DROPOUT)(out)

    out = Convolution2D(nb_filter2, kernel_size, kernel_size, border_mode='same',
                        dim_ordering=dim_ordering, name=conv_name_base + '2b', W_regularizer=l2(W_REG))(out)
    out = BatchNormalization(axis=bn_axis, name=bn_name_base + '2b')(out)
    out = Activation('relu')(out)
    if DROPOUT is not None:
        out = Dropout(DROPOUT)(out)

    out = Convolution2D(nb_filter3, 1, 1, dim_ordering=dim_ordering, name=conv_name_base + '2c', W_regularizer=l2(W_REG))(out)
    out = BatchNormalization(axis=bn_axis, name=bn_name_base + '2c')(out)
    if DROPOUT is not None:
        out = Dropout(DROPOUT)(out)

    shortcut = Convolution2D(nb_filter3, 1, 1, subsample=strides,
                             dim_ordering=dim_ordering, name=conv_name_base + '1', W_regularizer=l2(W_REG))(input_tensor)
    shortcut = BatchNormalization(axis=bn_axis, name=bn_name_base + '1')(shortcut)
    if DROPOUT is not None:
        shortcut = Dropout(DROPOUT)(shortcut)

    out = merge([out, shortcut], mode='sum')
    if DROPOUT is not None:
        out = Dropout(DROPOUT)(out)
    out = Activation('relu')(out)
    return out


def get_resnet50():
    if K.image_dim_ordering() == 'tf':
        inp = Input(shape=(224, 224, 3))
        bn_axis = 3
    else:
        inp = Input(shape=(3, 224, 224))
        bn_axis = 1

    dim_ordering = K.image_dim_ordering()
    out = ZeroPadding2D((3, 3), dim_ordering=dim_ordering)(inp)
    out = Convolution2D(64, 7, 7, subsample=(2, 2), dim_ordering=dim_ordering, name='conv1', W_regularizer=l2(W_REG))(out)
    out = BatchNormalization(axis=bn_axis, name='bn_conv1')(out)
    out = Activation('relu')(out)
    out = MaxPooling2D((3, 3), strides=(2, 2), dim_ordering=dim_ordering)(out)

    out = conv_block(out, 3, [64, 64, 256], stage=2, block='a', strides=(1, 1))
    out = identity_block(out, 3, [64, 64, 256], stage=2, block='b')
    out = identity_block(out, 3, [64, 64, 256], stage=2, block='c')

    out = conv_block(out, 3, [128, 128, 512], stage=3, block='a')
    out = identity_block(out, 3, [128, 128, 512], stage=3, block='b')
    out = identity_block(out, 3, [128, 128, 512], stage=3, block='c')
    out = identity_block(out, 3, [128, 128, 512], stage=3, block='d')

    out = conv_block(out, 3, [256, 256, 1024], stage=4, block='a')
    out = identity_block(out, 3, [256, 256, 1024], stage=4, block='b')
    out = identity_block(out, 3, [256, 256, 1024], stage=4, block='c')
    out = identity_block(out, 3, [256, 256, 1024], stage=4, block='d')
    out = identity_block(out, 3, [256, 256, 1024], stage=4, block='e')
    out = identity_block(out, 3, [256, 256, 1024], stage=4, block='f')

    out = conv_block(out, 3, [512, 512, 2048], stage=5, block='a')
    out = identity_block(out, 3, [512, 512, 2048], stage=5, block='b')
    out = identity_block(out, 3, [512, 512, 2048], stage=5, block='c')

    out = AveragePooling2D((7, 7), dim_ordering=dim_ordering)(out)
    out = Flatten()(out)
    out = Dense(1, activation='sigmoid', name='fc1000', W_regularizer=l2(W_REG))(out)

    model = Model(inp, out)

    return model

img_rows = img_cols = 224
train_data_dir = "train"
test_data_dir = "test"
validation_data_dir = "validation"
batch_size = 16
WEIGHTS_FN = "weights/a.hdf5"
RNG_SEED = 4892374
DROPOUT = None
NB_EPOCH = 70 # rely on early stopping!
LR = .000001
W_REG = .00001

sys.setrecursionlimit(10000)
random.seed(RNG_SEED)
np.random.seed(random.randint(0, 4294967295))

if __name__ == '__main__':
	resnet_model = get_resnet50()
	resnet_model.compile(loss="binary_crossentropy",
              optimizer=Adam(lr=LR),
              metrics=["accuracy"])
    
		
	train_datagen = ImageDataGenerator(
			horizontal_flip=True,
			vertical_flip=True,
			rescale=1/255)
			
	test_datagen = ImageDataGenerator(rescale=1/255)

	train_generator = train_datagen.flow_from_directory(
			train_data_dir,
			target_size=(img_rows, img_cols),
			batch_size=batch_size,
			class_mode="binary",
			shuffle=True,
		   )
			
	validation_generator = test_datagen.flow_from_directory(
			validation_data_dir,
			target_size=(img_rows, img_cols),
			batch_size=batch_size,
			class_mode="binary",
		   )

	test_generator = test_datagen.flow_from_directory(
			test_data_dir,
			target_size=(img_rows, img_cols),
			batch_size=batch_size,
			class_mode="binary",
		 )
	try:
		history = History()
		resnet_model.fit_generator(
			train_generator,
			samples_per_epoch=1260,
			nb_epoch=NB_EPOCH,
			validation_data=validation_generator,
			nb_val_samples=156,
			callbacks=[
		#		ModelCheckpoint(WEIGHTS_FN, monitor='val_loss', save_best_only=True, save_weights_only=True),
				history
				]
			)
	except KeyboardInterrupt:
		pass
	resnet_model.save_weights(WEIGHTS_FN)
	print(history.history)
	plt.plot(history.history["val_loss"], "r--")
	plt.plot(history.history["loss"], "b--")
	plt.show()
	
	
