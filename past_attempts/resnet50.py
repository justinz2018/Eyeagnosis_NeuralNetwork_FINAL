# ResNet-50, network from the paper:
# "Deep Residual Learning for Image Recognition"
# http://arxiv.org/pdf/1512.03385v1.pdf
# License: see https://github.com/KaimingHe/deep-residual-networks/blob/master/LICENSE

# Download pretrained weights from:
# https://s3.amazonaws.com/lasagne/recipes/pretrained/imagenet/resnet50.pkl

import lasagne
from lasagne.layers import InputLayer
from lasagne.layers import Conv2DLayer as ConvLayer
from lasagne.layers import BatchNormLayer
from lasagne.layers import Pool2DLayer as PoolLayer
from lasagne.layers import NonlinearityLayer
from lasagne.layers import ElemwiseSumLayer
from lasagne.layers import DenseLayer
from lasagne.nonlinearities import rectify, softmax, sigmoid
import theano.tensor as T


def build_simple_block(incoming_layer, names,
                       num_filters, filter_size, stride, pad,
                       use_bias=False, nonlin=rectify):
    """Creates stacked Lasagne layers ConvLayer -> BN -> (ReLu)
    Parameters:
    ----------
    incoming_layer : instance of Lasagne layer
        Parent layer
    names : list of string
        Names of the layers in block
    num_filters : int
        Number of filters in convolution layer
    filter_size : int
        Size of filters in convolution layer
    stride : int
        Stride of convolution layer
    pad : int
        Padding of convolution layer
    use_bias : bool
        Whether to use bias in conlovution layer
    nonlin : function
        Nonlinearity type of Nonlinearity layer
    Returns
    -------
    tuple: (net, last_layer_name)
        net : dict
            Dictionary with stacked layers
        last_layer_name : string
            Last layer name
    """
    net = []
    names = list(names)
    net.append((
            names[0],
            ConvLayer(incoming_layer, num_filters, filter_size, pad, stride,
                      flip_filters=False, nonlinearity=None) if use_bias
            else ConvLayer(incoming_layer, num_filters, filter_size, stride, pad, b=None,
                           flip_filters=False, nonlinearity=None)
        ))

    net.append((
            names[1],
            BatchNormLayer(net[-1][1])
        ))
    if nonlin is not None:
        net.append((
            names[2],
            NonlinearityLayer(net[-1][1], nonlinearity=nonlin)
        ))

    return dict(net), net[-1][0]


def build_residual_block(incoming_layer, ratio_n_filter=1.0, ratio_size=1.0, has_left_branch=False,
                         upscale_factor=4, ix=''):
    """Creates two-branch residual block
    Parameters:
    ----------
    incoming_layer : instance of Lasagne layer
        Parent layer
    ratio_n_filter : float
        Scale factor of filter bank at the input of residual block
    ratio_size : float
        Scale factor of filter size
    has_left_branch : bool
        if True, then left branch contains simple block
    upscale_factor : float
        Scale factor of filter bank at the output of residual block
    ix : int
        Id of residual block
    Returns
    -------
    tuple: (net, last_layer_name)
        net : dict
            Dictionary with stacked layers
        last_layer_name : string
            Last layer name
    """
    simple_block_name_pattern = ['res%s_branch%i%s', 'bn%s_branch%i%s', 'res%s_branch%i%s_relu']

    net = {}

    # right branch
    net_tmp, last_layer_name = build_simple_block(
        incoming_layer, map(lambda s: s % (ix, 2, 'a'), simple_block_name_pattern),
        int(lasagne.layers.get_output_shape(incoming_layer)[1]*ratio_n_filter), 1, int(1.0/ratio_size), 0)
    net.update(net_tmp)

    net_tmp, last_layer_name = build_simple_block(
        net[last_layer_name], map(lambda s: s % (ix, 2, 'b'), simple_block_name_pattern),
        lasagne.layers.get_output_shape(net[last_layer_name])[1], 3, 1, 1)
    net.update(net_tmp)

    net_tmp, last_layer_name = build_simple_block(
        net[last_layer_name], map(lambda s: s % (ix, 2, 'c'), simple_block_name_pattern),
        lasagne.layers.get_output_shape(net[last_layer_name])[1]*upscale_factor, 1, 1, 0,
        nonlin=None)
    net.update(net_tmp)

    right_tail = net[last_layer_name]
    left_tail = incoming_layer

    # left branch
    if has_left_branch:
        net_tmp, last_layer_name = build_simple_block(
            incoming_layer, map(lambda s: s % (ix, 1, ''), simple_block_name_pattern),
            int(lasagne.layers.get_output_shape(incoming_layer)[1]*4*ratio_n_filter), 1, int(1.0/ratio_size), 0,
            nonlin=None)
        net.update(net_tmp)
        left_tail = net[last_layer_name]

    net['res%s' % ix] = ElemwiseSumLayer([left_tail, right_tail], coeffs=1)
    net['res%s_relu' % ix] = NonlinearityLayer(net['res%s' % ix], nonlinearity=rectify)

    return net, 'res%s_relu' % ix


def build_model():
    net = {}
    net['input'] = InputLayer((None, 3, img_rows, img_cols), input_var=inputs)
    sub_net, parent_layer_name = build_simple_block(
        net['input'], ['conv1', 'bn_conv1', 'conv1_relu'],
        64, 7, 3, 2, use_bias=True)
    net.update(sub_net)
    net['pool1'] = PoolLayer(net[parent_layer_name], pool_size=3, stride=2, pad=0, mode='max', ignore_border=False)
    block_size = list('abc')
    parent_layer_name = 'pool1'
    for c in block_size:
        if c == 'a':
            sub_net, parent_layer_name = build_residual_block(net[parent_layer_name], 1, 1, True, 4, ix='2%s' % c)
        else:
            sub_net, parent_layer_name = build_residual_block(net[parent_layer_name], 1.0/4, 1, False, 4, ix='2%s' % c)
        net.update(sub_net)

    block_size = list('abcd')
    for c in block_size:
        if c == 'a':
            sub_net, parent_layer_name = build_residual_block(
                net[parent_layer_name], 1.0/2, 1.0/2, True, 4, ix='3%s' % c)
        else:
            sub_net, parent_layer_name = build_residual_block(net[parent_layer_name], 1.0/4, 1, False, 4, ix='3%s' % c)
        net.update(sub_net)

    block_size = list('abcdef')
    for c in block_size:
        if c == 'a':
            sub_net, parent_layer_name = build_residual_block(
                net[parent_layer_name], 1.0/2, 1.0/2, True, 4, ix='4%s' % c)
        else:
            sub_net, parent_layer_name = build_residual_block(net[parent_layer_name], 1.0/4, 1, False, 4, ix='4%s' % c)
        net.update(sub_net)

    block_size = list('abc')
    for c in block_size:
        if c == 'a':
            sub_net, parent_layer_name = build_residual_block(
                net[parent_layer_name], 1.0/2, 1.0/2, True, 4, ix='5%s' % c)
        else:
            sub_net, parent_layer_name = build_residual_block(net[parent_layer_name], 1.0/4, 1, False, 4, ix='5%s' % c)
        net.update(sub_net)
    net['pool5'] = PoolLayer(net[parent_layer_name], pool_size=7, stride=1, pad=0,
                             mode='average_exc_pad', ignore_border=False)
    net['fc1000'] = DenseLayer(net['pool5'], num_units=2, nonlinearity=None)
    net['prob'] = NonlinearityLayer(net['fc1000'], nonlinearity=softmax)

    return net

from keras.preprocessing.image import ImageDataGenerator
from lasagne.layers import get_all_params, get_output, get_all_param_values
import theano
import glob
import numpy as np
import pickle
import matplotlib.pyplot as plt
import sys
from keras.utils import np_utils

inputs = T.tensor4("inputs")
targets = T.ivector("targets")

#LAMBDA = 0.005
nb_epoch = 25
batch_size = 64
img_rows = img_cols = 224
train_data_dir = "train"
validation_data_dir = "validation"
test_data_dir = "test"
plots_dir = "plots"

if __name__ == "__main__":
    network = build_model()['prob']
#    print(network)
    params = get_all_params(network, trainable=True)
    predictions = get_output(network)
    costs = lasagne.objectives.categorical_crossentropy(predictions, targets)
    cost = costs.mean()
 #   cost += LAMBDA * lasagne.regularization.regularize_network_params(
 #   	network, lasagne.regularization.l2)
    updates = lasagne.updates.adadelta(cost, params)
    train = theano.function(inputs=[inputs, targets],
    						outputs=[cost, predictions],
    						updates=updates, allow_input_downcast=True)
    predict = theano.function(inputs=[inputs, targets],
    						outputs=[cost, predictions], allow_input_downcast=True)

    train_datagen = ImageDataGenerator(
    #        samplewise_std_normalization=True,
    #        samplewise_center=True,
    #        shear_range=0.3,
     #       color_shift_range=234.0
    #        rotation_range=180,
            horizontal_flip=True,
            vertical_flip=True,
            rescale=1/255)

    test_datagen = ImageDataGenerator(
    #        samplewise_std_normalization=True,
    #        samplewise_center=True,
            rescale=1/255)

    train_generator = train_datagen.flow_from_directory(
            train_data_dir,
            target_size=(img_rows, img_cols),
            batch_size=batch_size,
            class_mode="binary",
        #    color_mode="grayscale"
           )

    validation_generator = test_datagen.flow_from_directory(
            validation_data_dir,
            target_size=(img_rows, img_cols),
            batch_size=batch_size,
            class_mode="binary",
        #    color_mode="grayscale"
           )

    test_generator = test_datagen.flow_from_directory(
            test_data_dir,
            target_size=(img_rows, img_cols),
            batch_size=batch_size,
            class_mode="binary",
        #    color_mode="grayscale"
         )

    nb_train = len(list(glob.glob("train/**/*.jpeg", recursive=True)))
    nb_validation = len(list(glob.glob("validation/**/*.jpeg", recursive=True)))
    for e in range(nb_epoch):
        print("-------------------------------------")
        print("EPOCH {}/{}".format(e+1, nb_epoch))
        count = 0
        tot_cost = tot_acc = 0
        for x, y in train_generator:
            cost, predictions = train(x, y)
            count += len(x)
            #print(y)
            #print(predictions)
            #print(predictions.argmax(axis=1))
            #print(predictions.argmax(axis=1) == y)
            acc = np.mean(predictions.argmax(axis=1) == y)
            #acc = np.mean((predictions >= .5) == y)
            sys.stdout.write("COST: %-18.15s ACC: %-15s %8s/%-8s \r" %
                (cost, acc, count, nb_train))
            sys.stdout.flush()
            tot_cost += cost*len(x)
            tot_acc += acc*len(x)
            if count >= nb_train: break
        sys.stdout.write("TRAIN COST: %-18.15s TRAIN ACC: %-15s %8s/%-8s \r" %
            (tot_cost/nb_train, tot_acc/nb_train, count, nb_train))
        sys.stdout.flush()
        print()
        tot_cost = tot_acc = count = 0
        for x, y in validation_generator:
            cost, predictions = predict(x, y)
            tot_cost += cost*len(x)
            tot_acc += np.mean(predictions.argmax(axis=1) == y)*len(x)
            #tot_acc += np.mean((predictions >= .5) == y)*len(x)
            count += len(x)
            if count >= nb_validation: break
        tot_cost /= nb_validation
        tot_acc /= nb_validation
        print("VAL COST: {}, VAL ACC: {}".format(tot_cost, tot_acc))
        data = get_all_param_values(network)
        with open("weights/{}.weights".format(e), "wb") as f:
                pickle.dump(data, f)
	#visualize.plot_conv_weights(get_all_layers(network)[1]).savefig(plots_dir+"/1/{}.png".format(e+1))
    	#visualize.plot_conv_weights(get_all_layers(network)[2]).savefig(plots_dir+"/2/{}.png".format(e+1))
    	#plt.close()

pred = []
actual = []
for X,Y in validation_generator:
	Y_pred = model.predict(X, batch_size=len(X)).tolist()
	pred += Y_pred.argmax(axis=1)
	actual += Y.tolist()
	if len(pred) >= nb_test_samples:
		break
assert(len(pred) == len(actual))
same = 0
for i in range(len(pred)):
	if abs(pred[i]-actual[i]) <= .000005:
		same += 1
#print("Test score: {}".format(score[0]))
print("Test accuracy: {}".format(same/len(pred)))
numone = 0
for i in actual:
	if i >= .99999:
		numone += 1
pred_numone = 0
for i in pred:
	if i >= .99999:
		pred_numone += 1
print("numone: ",numone)
print("pred_numone: ",pred_numone)
print(pred[:20])
print(actual[:20])

print("KAPPA: ",quadratic_weighted_kappa(pred, actual))
