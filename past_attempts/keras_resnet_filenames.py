from __future__ import print_function
import random, glob, sys

import keras
from keras.layers import merge
from sklearn.utils.class_weight import compute_class_weight
from convnets import *
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.convolutional import Convolution2D, MaxPooling2D, ZeroPadding2D, AveragePooling2D
from keras.layers.core import Dense, Activation, Flatten, Dropout, Lambda
from keras.layers.normalization import BatchNormalization
from keras.models import Model
from keras.layers import Input
from keras.optimizers import Adam, SGD
from keras.preprocessing.image import load_img, img_to_array
import keras.backend as K
from keras.metrics import binary_crossentropy
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import EarlyStopping, ModelCheckpoint, History, LearningRateScheduler
from keras.regularizers import l2
import numpy as np
import matplotlib.pyplot as plt
import theano.tensor as T

from heatmap import to_heatmap

class LogHistory(keras.callbacks.Callback):
	def on_epoch_end(self, epoch, logs={}):
		f = open("plots_"+NAME+"/{}.history".format(epoch+194), "w")
		f.write(str(logs))
		f.close()
		
NAME = "resnet_real"

img_rows = img_cols = IMG_ROWS = IMG_COLS = 224
train_data_dir = "train"
test_data_dir = "test_real"
validation_data_dir = "validation"
batch_size = 16
WEIGHTS_FN = "weights_"+NAME+"/{epoch:03d}-{val_loss:.2f}.hdf5"
RNG_SEED = 123454321
DROPOUT = .4
NB_EPOCH = 100000 # rely on early stopping!
LR = 0.0003333
LR_SCHEDULE = {}
W_REG = 1.0
ALPHA = .02


def scheduler(epoch):
	global LR
	if epoch in LR_SCHEDULE:
		LR = LR_SCHEDULE[epoch]
	return LR

total_train_0 = len(glob.glob(train_data_dir+"/0/*.jpeg", recursive=True))
total_train = len(glob.glob(train_data_dir+"/**/*.jpeg", recursive=True))
total_val = len(glob.glob(validation_data_dir+"/**/*.jpeg", recursive=True))
total_test = len(glob.glob(test_data_dir+"/**/*.jpeg", recursive=True))

sys.setrecursionlimit(10000)
random.seed(RNG_SEED)
np.random.seed(random.randint(0, 4294967295))

from keras.preprocessing import image

def display_heatmap(new_model, img_path):
	img = image.load_img(img_path, target_size=(224, 224))
	x = image.img_to_array(img)
	x = np.expand_dims(x, axis=0)
	out = new_model.predict(x)
	plt.imshow(out[0, 1].sum(axis=0), interpolation = "none")
	plt.savefig('heatmap.png')

def clip(inp):
	return K.clip(inp, 0, 1)
	
def normalize(img):
	img -= K.mean(img)
	img /= K.std(img)
	return img
def ninety(mat):
	import theano
	theano.tensor.set_subtensor(mat[0], K.transpose(mat[0][::-1]))
	theano.tensor.set_subtensor(mat[1], K.transpose(mat[1][::-1]))
	theano.tensor.set_subtensor(mat[2], K.transpose(mat[2][::-1]))
	return mat
def one_eighty(mat):
	return mat[:, ::-1]
def neg_ninety(mat):
	import theano
	theano.tensor.set_subtensor(mat[0], K.transpose(mat[0])[::-1])
	theano.tensor.set_subtensor(mat[1], K.transpose(mat[1])[::-1])
	theano.tensor.set_subtensor(mat[2], K.transpose(mat[2])[::-1])
	return mat

def f1_score(y_true, y_pred):
	y_pred = K.round(y_pred)
	tp = K.sum(K.equal(y_pred, 1) * K.equal(y_true, 1))
	fp = K.sum(K.equal(y_pred, 1) * K.equal(y_true, 0))
	fn = K.sum(K.equal(y_pred, 0) * K.equal(y_true, 1))
	precision = K.switch(K.equal(tp, 0), 0, tp/(tp+fp))
	recall = K.switch(K.equal(tp, 0), 0, tp/(tp+fn))
	return K.switch(K.equal(precision+recall, 0), 0, 2*(precision*recall)/(precision+recall))

#if __name__ == '__main__':
if True:
	internal = ResNet_50()
	for layer in internal.layers:
		layer.W_regularizer = l2(W_REG)
	inp2 = Input((3, IMG_ROWS, IMG_COLS))
	model = Lambda(normalize)(inp2)
	b = Lambda(ninety)(model)
	c = Lambda(one_eighty)(model)
	d = Lambda(neg_ninety)(model)
	a = internal(model)
	b = internal(b)
	c = internal(c)
	d = internal(d)
	if DROPOUT is not None:
		a = Dropout(DROPOUT)(a)
		b = Dropout(DROPOUT)(b)
		c = Dropout(DROPOUT)(c)
		d = Dropout(DROPOUT)(d)
	model = merge([a,b,c,d], mode="ave")
	model = Dense(1024, input_shape=(None,1000), W_regularizer=l2(W_REG))(model)
	model = LeakyReLU(ALPHA)(model)
	model = Dense(512, input_shape=(None,1024), W_regularizer=l2(W_REG))(model)
	model = LeakyReLU(ALPHA)(model)
	model = Dense(1, input_shape=(None,512), W_regularizer=l2(W_REG))(model)
	model = Activation("sigmoid")(model)
	#model = LeakyReLU(ALPHA)(model)
	#model = Lambda(clip, output_shape=lambda x:x)(model)
	model = Model(inp2, model)
	model.summary()
	for layer in model.layers:
		layer.W_regularizer = l2(W_REG)
	model.compile(loss="binary_crossentropy",
		#	optimizer=SGD(lr=LR, momentum=.9, nesterov=True),
			optimizer=Adam(lr=LR),
			metrics=["accuracy", f1_score])
	model.load_weights("/home/justin/Downloads/FINAL_WEIGHTS.hdf5")
	test_datagen = ImageDataGenerator()
	test_generator = test_datagen.flow_from_directory(
			test_data_dir,
			target_size=(img_rows, img_cols),
			batch_size=batch_size,
			class_mode="binary",
		 )
	y_pred = []
	y_true = []
	for x, y in test_generator:
		y_pred += model.predict_on_batch(x).tolist()
		y_true += y.tolist()
		if len(y_pred) >= total_test:
			break
	f = open("plots_"+NAME+"/final_eval_pred.txt", "w")
	f.write(str(y_pred))
	f.close()
	f = open("plots_"+NAME+"/final_eval_true.txt", "w")
	f.write(str(y_true))
	f.close()
	f = open("plots_"+NAME+"/fileclasses.txt","w")
	f.write(str(test_generator.filenames))
	print("we have results!")
	'''
	new_model = to_heatmap(Model(inp2,a))
	display_heatmap(new_model, 'test/1/1965_left.jpeg')

	scores = model.evaluate_generator(test_generator, val_samples=total_test)


	print(scores)
	'''
