from __future__ import print_function
import random, glob, sys

from keras.layers import merge
from keras.layers.convolutional import Convolution2D, MaxPooling2D, ZeroPadding2D, AveragePooling2D
from keras.layers.core import Dense, Activation, Flatten, Dropout, Lambda, MaxoutDense
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.normalization import BatchNormalization
from keras.models import Model, Sequential, load_model
from keras.layers import Input
from keras.optimizers import Adam
from keras.preprocessing.image import load_img, img_to_array
import keras.backend as K
from keras.metrics import binary_crossentropy
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import EarlyStopping, ModelCheckpoint, History
from keras.regularizers import l2
import keras
import numpy as np
import matplotlib.pyplot as plt


class LogHistory(keras.callbacks.Callback):
	def on_epoch_end(self, epoch, logs={}):
		f = open("plots_own/{}.history".format(epoch+13), "w")
		f.write(str(logs))
		f.close()

img_rows = img_cols = 224
train_data_dir = "train"
test_data_dir = "test"
validation_data_dir = "validation"
batch_size = 16
nb_filters = 32
WEIGHTS_FN = "weights_own/{epoch:02d}-{val_loss:.2f}.hdf5"
RNG_SEED = 12345678
DROPOUT = .5
NB_EPOCH = 200 # rely on early stopping!
LR = .00001
W_REG = .00001
ALPHA = 0.01
nb_pool = 2

af_ratio = len(glob.glob(train_data_dir+"/0/*.jpeg", recursive=True)) / len(glob.glob(train_data_dir+"/1/*.jpeg", recursive=True))
total_train = len(glob.glob(train_data_dir+"/**/*.jpeg", recursive=True))
total_val = len(glob.glob(validation_data_dir+"/**/*.jpeg", recursive=True))
total_test = len(glob.glob(test_data_dir+"/**/*.jpeg", recursive=True))

sys.setrecursionlimit(10000)
random.seed(RNG_SEED)
np.random.seed(random.randint(0, 4294967295))

def scaled_binary_crossentropy(y_true, y_pred):
	cost = binary_crossentropy(y_true, y_pred)
	mult = y_true*(af_ratio-1) + 1
	return cost * mult
	
def f1_score(y_true, y_pred):
	y_pred = K.round(y_pred)
	tp = K.sum(K.equal(y_pred, 1) * K.equal(y_true, 1))
	fp = K.sum(K.equal(y_pred, 1) * K.equal(y_true, 0))
	fn = K.sum(K.equal(y_pred, 0) * K.equal(y_true, 1))
	precision = K.switch(K.equal(tp, 0), 0, tp/(tp+fp))
	recall = K.switch(K.equal(tp, 0), 0, tp/(tp+fn))
	return K.switch(K.equal(precision+recall, 0), 0, 2*(precision*recall)/(precision+recall))

if __name__ == '__main__':
	model = Sequential()
	model.add(Convolution2D(32, 5, 5, border_mode='same', input_shape=(3, img_rows, img_cols), W_regularizer=l2(W_REG)))
	model.add(LeakyReLU(alpha=ALPHA))
	model.add(Convolution2D(32, 3, 3, border_mode='same', W_regularizer=l2(W_REG)))
	model.add(LeakyReLU(alpha=ALPHA))
	
	model.add(MaxPooling2D(pool_size=(3, 3), strides=(2, 2)))
	
	model.add(Convolution2D(64, 5, 5, border_mode='same', W_regularizer=l2(W_REG)))
	model.add(LeakyReLU(alpha=ALPHA))
	model.add(Convolution2D(64, 3, 3, border_mode='same', W_regularizer=l2(W_REG)))
	model.add(LeakyReLU(alpha=ALPHA))
	model.add(Convolution2D(64, 3, 3, border_mode='same', W_regularizer=l2(W_REG)))
	model.add(LeakyReLU(alpha=ALPHA))
	
	model.add(MaxPooling2D(pool_size=(3, 3), strides=(2, 2)))

	model.add(Convolution2D(128, 3, 3, border_mode='same', W_regularizer=l2(W_REG)))
	model.add(LeakyReLU(alpha=ALPHA))
	model.add(Convolution2D(128, 3, 3, border_mode='same', W_regularizer=l2(W_REG)))
	model.add(LeakyReLU(alpha=ALPHA))
	model.add(Convolution2D(128, 3, 3, border_mode='same', W_regularizer=l2(W_REG)))
	model.add(LeakyReLU(alpha=ALPHA))
	
	model.add(MaxPooling2D(pool_size=(3, 3), strides=(2, 2)))

	model.add(Convolution2D(256, 3, 3, border_mode='same', W_regularizer=l2(W_REG)))
	model.add(LeakyReLU(alpha=ALPHA))
	model.add(Convolution2D(256, 3, 3, border_mode='same', W_regularizer=l2(W_REG)))
	model.add(LeakyReLU(alpha=ALPHA))
	model.add(Convolution2D(256, 3, 3, border_mode='same', W_regularizer=l2(W_REG)))
	model.add(LeakyReLU(alpha=ALPHA))
	
	model.add(MaxPooling2D(pool_size=(3, 3), strides=(2, 2)))
	
	model.add(Convolution2D(512, 3, 3, border_mode='same', W_regularizer=l2(W_REG)))
	model.add(LeakyReLU(alpha=ALPHA))
	model.add(Convolution2D(512, 3, 3, border_mode='same', W_regularizer=l2(W_REG)))
	model.add(LeakyReLU(alpha=ALPHA))
		
	model.add(MaxPooling2D(pool_size=(2, 2)))
	if DROPOUT:
		model.add(Dropout(DROPOUT))
	#model.add(MaxPooling2D(pool_size=(nb_pool, nb_pool)))

	model.add(Flatten())
	model.add(Dense(1024, W_regularizer=l2(W_REG)))
	model.add(LeakyReLU(alpha=ALPHA))
	model.add(MaxoutDense(512, W_regularizer=l2(W_REG)))
	if DROPOUT:
		model.add(Dropout(DROPOUT))
	
	model.add(Dense(1024, W_regularizer=l2(W_REG)))
	model.add(LeakyReLU(alpha=ALPHA))
	model.add(MaxoutDense(512, W_regularizer=l2(W_REG)))
	
	model.add(Dense(1))
	model.add(Activation("sigmoid"))
	
	model.summary()
	
	model.compile(loss="binary_crossentropy",
              optimizer=Adam(lr=LR),
              metrics=["accuracy", f1_score])
    
	#model = load_model("weights_own/02-1.03.hdf5", {"scaled_binary_crossentropy":scaled_binary_crossentropy, "f1_score":f1_score})
	train_datagen = ImageDataGenerator(
			horizontal_flip=True,
			vertical_flip=True,
			samplewise_center=True,
			samplewise_std_normalization=True,
			rotation_range=90,
			fill_mode="constant",
			channel_shift_range=30,
			cval=0,
			shear_range=.2
			)
			
	test_datagen = ImageDataGenerator(
			samplewise_center=True,
			samplewise_std_normalization=True)

	train_generator = train_datagen.flow_from_directory(
			train_data_dir,
			target_size=(img_rows, img_cols),
			batch_size=batch_size,
			class_mode="binary",
			shuffle=True,
	#		save_to_dir="plots_own/test_img"
		   )
			
	validation_generator = test_datagen.flow_from_directory(
			validation_data_dir,
			target_size=(img_rows, img_cols),
			batch_size=batch_size,
			class_mode="binary",
		   )

	test_generator = test_datagen.flow_from_directory(
			test_data_dir,
			target_size=(img_rows, img_cols),
			batch_size=batch_size,
			class_mode="binary",
		 )
	try:
		history = History()
		model.fit_generator(
			train_generator,
			samples_per_epoch=total_train,
			nb_epoch=NB_EPOCH,
			validation_data=validation_generator,
			nb_val_samples=total_val,
			callbacks=[
				ModelCheckpoint(WEIGHTS_FN),
				history,
				LogHistory()
				]
			)
	except KeyboardInterrupt:
		pass
	print(history.history)
	f = open("plots_own/history.txt", "w")
	f.write(str(history.history))
	f.close()
	model.save_weights(WEIGHTS_FN)
	try:
		val_loss, = plt.plot(history.history["val_loss"], "r-", label="Validation Loss")
		loss, = plt.plot(history.history["loss"], "b-", label="Training Loss")
		plt.title("Loss scores (scaled binary crossentropy)")
		plt.legend(handles=[val_loss, loss])
		plt.savefig("plots_own/loss_fig.png")
		plt.close()
		val_f1, = plt.plot(history.history["val_f1_score"], "r-", label="Validation F1")
		f1, = plt.plot(history.history["f1_score"], "b-", label="Test F1")
		plt.title("F1 scores")
		plt.legend(handles=[val_f1, f1])
		plt.savefig("plots_own/f1_fig.png")
		plt.close()
	except:
		pass
	scores = model.evaluate_generator(test_generator, val_samples=total_test)
	print(scores)
