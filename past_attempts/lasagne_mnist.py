import sys
import glob
import os

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import lasagne
from lasagne.layers import *
from lasagne.nonlinearities import *
import theano
import theano.tensor as T
import numpy as np

np.random.seed(1337)

from nolearn.lasagne import BatchIterator
from nolearn.lasagne import visualize
from keras.preprocessing.image import ImageDataGenerator
from keras.datasets import mnist

inputs = T.tensor4("inputs")
targets = T.ivector("targets")

LAMBDA = 0.001
nb_epoch = 12
batch_size = 128
img_rows = img_cols = 28
train_data_dir = "train"
validation_data_dir = "validation"
test_data_dir = "test"
plots_dir = "plots"

network = InputLayer(shape=(None, 1, img_rows, img_cols), input_var=inputs)

network = Conv2DLayer(network, num_filters=32, filter_size=(5, 5),
										nonlinearity=rectify)
network = Conv2DLayer(network, num_filters=32, filter_size=(3, 3),
										nonlinearity=rectify)
network = MaxPool2DLayer(network, pool_size=(2, 2))
'''
network = Conv2DLayer(network, num_filters=32, filter_size=(5, 5),
										nonlinearity=LeakyRectify(.01))
network = Conv2DLayer(DropoutLayer(network, .5), num_filters=32, filter_size=(3, 3),
										nonlinearity=LeakyRectify(.01))
#network = MaxPool2DLayer(DropoutLayer(network, .1), pool_size=(2, 2))
'''
network = DenseLayer(DropoutLayer(network, .25), num_units=256, nonlinearity=rectify)
network = DenseLayer(DropoutLayer(network, .5), num_units=10, nonlinearity=softmax)

params = get_all_params(network, trainable=True)
predictions = get_output(network)
costs = lasagne.objectives.categorical_crossentropy(predictions, targets)
cost = costs.mean()
cost += LAMBDA * lasagne.regularization.regularize_network_params(
	network, lasagne.regularization.l2)
updates = lasagne.updates.adadelta(cost, params)
train = theano.function(inputs=[inputs, targets],
						outputs=[cost, predictions],
						updates=updates, allow_input_downcast=True)
predict = theano.function(inputs=[inputs, targets],
						outputs=[cost, predictions], allow_input_downcast=True)

(X_train, y_train), (X_test, y_test) = mnist.load_data()

X_train = X_train.reshape(X_train.shape[0], 1, img_rows, img_cols)
X_test = X_test.reshape(X_test.shape[0], 1, img_rows, img_cols)
X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
X_train /= 255
X_test /= 255
bi = BatchIterator(batch_size=batch_size, shuffle=True)

nb_train = len(X_train)
nb_validation = len(X_test)

for e in range(nb_epoch):
	print("-------------------------------------")
	print("EPOCH {}/{}".format(e+1, nb_epoch))
	count = 0
	tot_cost = tot_acc = 0
	for x, y in bi(X_train, y_train):
		cost, predictions = train(x, y)
		count += len(x)
		acc = np.mean(predictions.argmax(axis=1) == y)
		sys.stdout.write("COST: %-18.15s ACC: %-15s %8s/%-8s \r" %
			(cost, acc, count, nb_train))
		sys.stdout.flush()
		tot_cost += cost*len(x)
		tot_acc += acc*len(x)
	sys.stdout.write("TRAIN COST: %-18.15s TRAIN ACC: %-15s %8s/%-8s \r" %
		(tot_cost/nb_train, tot_acc/nb_train, count, nb_train))
	sys.stdout.flush()
	print()
	tot_cost = tot_acc = count = 0
	for x, y in bi(X_test, y_test):
		cost, predictions = predict(x, y)
		tot_cost += cost*len(x)
		tot_acc += np.mean(predictions.argmax(axis=1) == y)*len(x)
		count += len(x)
	tot_cost /= nb_validation
	tot_acc /= nb_validation
	print("VAL COST: {}, VAL ACC: {}".format(tot_cost, tot_acc))
	visualize.plot_conv_weights(get_all_layers(network)[1]).savefig(plots_dir+"/1/{}.png".format(e+1))
	visualize.plot_conv_weights(get_all_layers(network)[2]).savefig(plots_dir+"/2/{}.png".format(e+1))
	plt.close()
