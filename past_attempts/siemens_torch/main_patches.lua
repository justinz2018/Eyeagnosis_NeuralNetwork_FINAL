require("torch")
require("nn")
require("xlua")
require("optim")
require("cunn")
require("rnn")
require("cudnn")
local dl = require("dataload")

opt = {["datapath"]="../siemens/train", ["img_multiplier"]=8,
		["learningRate"]=.01, ["epochs"]=100000, ["cuda"]=true, ["batch_size"]=6}

dataloader = dl.ImageClass(opt.datapath, {3, opt.img_multiplier*256, opt.img_multiplier*256})
model = nn.Sequential()

tmp = nn.Parallel(2, 1)
--[[inner = nn.Sequential()
inner:add(nn.SpatialConvolution(3, 16, 5, 5))
inner:add(nn.ReLU())
inner:add(nn.SpatialMaxPooling(2, 2))
inner:add(nn.SpatialConvolution(16, 16, 5, 5))
inner:add(nn.ReLU())
inner:add(nn.SpatialMaxPooling(2, 2))
inner:add(nn.SpatialConvolution(16, 16, 5, 5))
inner:add(nn.ReLU())
inner:add(nn.SpatialMaxPooling(2, 2))
inner:add(nn.SpatialConvolution(16, 16, 6, 6))
inner:add(nn.ReLU())
inner:add(nn.SpatialMaxPooling(2, 2))
inner:add(nn.Reshape(16*11*11))
inner:add(nn.Linear(16*11*11, 1))
]]--
inner = torch.load("resnet-50.t7")
inner:add(nn.Linear(1000, 1))
if opt.cuda then
	inner:cuda()
end
for i=1,opt.img_multiplier^2 do
	tmp:add(inner)
end
model:add(tmp)

model:add(nn.Reshape(opt.img_multiplier^2, opt.batch_size))
model:add(nn.Transpose({1, 2}))

model:add(nn.Linear(opt.img_multiplier^2, 30))
model:add(nn.Linear(30, 2))
model:add(nn.LogSoftMax())

local min1 = 1000000000
weights = torch.Tensor(#dataloader.classList)
for i=1,#dataloader.classList do
	min1 = math.min(min1, dataloader.classList[i]:size(1))
	weights[i] = 1/dataloader.classList[i]:size(1)
end
for i=1,#dataloader.classList do
	weights[i] = weights[i] * min1
end
crit = nn.ClassNLLCriterion(weights)

if opt.cuda then
	model:cuda()
	crit:cuda()
end

param,gradParam = model:getParameters()

--dataloader = dl.TensorLoader(inputs, targets)
for i=1,opt.epochs do
	t = optim.ConfusionMatrix(#dataloader.classes)
	t:zero()
	local count = 0
	for k, inputs, targets in dataloader:sampleiter(opt.batch_size) do
		count = count + 1
		inputs = inputs:double()
		r_input = torch.Tensor(inputs:size(1), opt.img_multiplier^2, 3, 256, 256)
		for i = 1,inputs:size(1) do
			for c = 1,3 do
				inputs[i][c]:add(-torch.mean(inputs[i][c]))
				inputs[i][c]:div(torch.std(inputs[i][c]))
				for j = 1,opt.img_multiplier do
					for k = 1,opt.img_multiplier do
						r_input[i][(j-1)*opt.img_multiplier+k][c] = inputs[i][c][{{(j-1)*256+1, j*256}, {(k-1)*256+1, k*256}}]
					end
				end
			end
		end
		
		-- random flip pls
		targets = targets:double()
		if opt.cuda then
			r_input = r_input:cuda()
			targets = targets:cuda()
		end
		local func = function(x)
			collectgarbage()
			param:copy(x)
			gradParam:zero()
			local out = model:forward(r_input)
			t:batchAdd(out, targets)
			local loss = crit:forward(out, targets)
			local df_dx = crit:backward(out, targets)
			model:backward(r_input, df_dx)
			
			xlua.progress(k, dataloader:size())
			if count % 3 == 0 then
				print(t)
			end
			--io.write(" ", torch.cmul(t.mat:double(), torch.eye(#dataloader.classes)):sum() / t.mat:sum())

			return loss, gradParam
		end
		optim.adam(func, param, {
				learningRate = opt.learningRate,
				verbose = true
			 })
		io.write("\r")
	end
	print(t)
end
