'''This script demonstrates how to build a variational autoencoder with Keras and deconvolution layers.

Reference: "Auto-Encoding Variational Bayes" https://arxiv.org/abs/1312.6114
'''
import numpy as np
import matplotlib.pyplot as plt

from keras.layers import Input, Dense, Lambda, Flatten, Reshape
from keras.layers import Convolution2D, Deconvolution2D, MaxPooling2D
from keras.models import Model, load_model
from keras import backend as K
from keras import objectives
import random, glob, sys

from keras.layers import merge
from keras.layers.convolutional import Convolution2D, MaxPooling2D, ZeroPadding2D, AveragePooling2D
from keras.layers.core import Dense, Activation, Flatten, Dropout
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.normalization import BatchNormalization
from keras.models import Model, Sequential, load_model
from keras.layers import Input
from keras.optimizers import Adam
from keras.preprocessing.image import load_img, img_to_array
import keras.backend as K
from keras.metrics import binary_crossentropy
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import EarlyStopping, ModelCheckpoint, History
from keras.regularizers import l2
import keras
import numpy as np
import matplotlib.pyplot as plt

def norm(img):
	img -= K.mean(img)
	img /= K.std(img)
	return img


train_data_dir = "train"
test_data_dir = "test"
validation_data_dir = "validation"

batch_size = 16
WEIGHTS_FN = "weights_vae/{epoch:02d}-{val_loss:.2f}.hdf5"
RNG_SEED = 12345678
DROPOUT = .0
NB_EPOCH = 200 # rely on early stopping!
LR = .000033333
W_REG = .000000

sys.setrecursionlimit(10000)
random.seed(RNG_SEED)
np.random.seed(random.randint(0, 4294967295))

af_ratio = len(glob.glob(train_data_dir+"/0/*.jpeg", recursive=True)) / len(glob.glob(train_data_dir+"/1/*.jpeg", recursive=True))
total_train = len(glob.glob(train_data_dir+"/**/*.jpeg", recursive=True))
total_val = len(glob.glob(validation_data_dir+"/**/*.jpeg", recursive=True))
total_test = len(glob.glob(test_data_dir+"/**/*.jpeg", recursive=True))

# input image dimensions
img_rows, img_cols, img_chns = 224, 224, 1
# number of convolutional filters to use
nb_filters = 32
# convolution kernel size
nb_conv = 3

original_dim = (img_chns, img_rows, img_cols)
latent_dim = 1200
intermediate_dim = 128
epsilon_std = 0.01
nb_epoch = 500


x = Input(batch_shape=(batch_size,) + original_dim)
y = Lambda(norm)(x)
c = Convolution2D(nb_filters, nb_conv, nb_conv, border_mode='same', activation='relu')(y)
f = Flatten()(c)
h = Dense(intermediate_dim, activation='relu')(f)

z_mean = Dense(latent_dim)(h)
z_log_var = Dense(latent_dim)(h)


def sampling(args):
    z_mean, z_log_var = args
    epsilon = K.random_normal(shape=(batch_size, latent_dim),
                              mean=0., std=epsilon_std)
    return z_mean + K.exp(z_log_var) * epsilon

# note that "output_shape" isn't necessary with the TensorFlow backend
# so you could write `Lambda(sampling)([z_mean, z_log_var])`
z = Lambda(sampling, output_shape=(latent_dim,))([z_mean, z_log_var])

# we instantiate these layers separately so as to reuse them later
decoder_h = Dense(intermediate_dim, activation='relu')
decoder_f = Dense(nb_filters*img_rows*img_cols, activation='relu')
decoder_c = Reshape((nb_filters, img_rows, img_cols))
decoder_mean = Deconvolution2D(img_chns, nb_conv, nb_conv,
                               (batch_size, img_chns, img_rows, img_cols),
                               border_mode='same')

h_decoded = decoder_h(z)
f_decoded = decoder_f(h_decoded)
c_decoded = decoder_c(f_decoded)
x_decoded_mean = decoder_mean(c_decoded)


def vae_loss(x, x_decoded_mean):
    # NOTE: binary_crossentropy expects a batch_size by dim for x and x_decoded_mean, so we MUST flatten these!
    x = K.flatten(x)
    x_decoded_mean = K.flatten(x_decoded_mean)
    xent_loss = objectives.binary_crossentropy(x, x_decoded_mean)
    kl_loss = - 0.5 * K.mean(1 + z_log_var - K.square(z_mean) - K.exp(z_log_var), axis=-1)
    return xent_loss + kl_loss

vae = Model(x, x_decoded_mean)
vae.compile(optimizer='rmsprop', loss=vae_loss)
vae.summary()
#vae.load_weights("weights_vae/s.hdf5")

encoder = Model(x, z_mean)


# build a model to project inputs on the latent space



train_datagen = ImageDataGenerator(
		horizontal_flip=True,
		vertical_flip=True,
		#samplewise_center=True,
		#samplewise_std_normalization=True,
		)
		
test_datagen = ImageDataGenerator(
		#samplewise_center=True,
		#samplewise_std_normalization=True
		)

train_generator = train_datagen.flow_from_directory(
		train_data_dir,
		target_size=(img_rows, img_cols),
		batch_size=batch_size,
		class_mode="binary",
		shuffle=True,
		color_mode="grayscale"
#		save_to_dir="plots_own/test_img"
	   )
		
validation_generator = test_datagen.flow_from_directory(
		validation_data_dir,
		target_size=(img_rows, img_cols),
		batch_size=batch_size,
		class_mode="binary",
		color_mode="grayscale"
	   )

test_generator = test_datagen.flow_from_directory(
		test_data_dir,
		target_size=(img_rows, img_cols),
		batch_size=batch_size,
		class_mode="binary",
		color_mode="grayscale"
	 )
try:
	c = 0
	d = 0
	for (x, _) in train_generator:
		c += 1
		if len(x) != batch_size:
			d += 1
			print(d)
			continue
		print(vae.train_on_batch(x, x))
		if c % 1000 == 0:
			print("-----------------{}".format(c))
			vae.save("weights_vae/s.hdf5")
except KeyboardInterrupt:
	pass
vae.save("weights_vae/s.hdf5")
encoder.save("weights_vae/encoder.hdf5")

def load_encoder():
	encoder.load_weights("weights_vae/encoder.hdf5")
	return encoder
