import glob, random, sys

import keras
from keras.layers import merge
from keras.layers import Input, Dense, Lambda, Flatten, Reshape
from keras.layers.convolutional import Convolution2D, MaxPooling2D, ZeroPadding2D, AveragePooling2D
from keras.layers.core import Dense, Activation, Flatten, Dropout
from keras.layers.normalization import BatchNormalization
from keras.layers.advanced_activations import LeakyReLU
from keras.models import Model, Sequential, load_model
from keras.layers import Input
from keras.optimizers import Adam, SGD
from keras.preprocessing.image import load_img, img_to_array
import keras.backend as K
from keras.metrics import binary_crossentropy
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import EarlyStopping, ModelCheckpoint, History
from keras.regularizers import l2
import numpy as np
import matplotlib.pyplot as plt
import theano.tensor as T

IMG_ROWS = IMG_COLS = 768
LR = 1e-3
reg = l2(5e-5)
ALPHA = .1
batch_size = 8
train_data_dir = "train"
test_data_dir = "test"
validation_data_dir = "validation"
af_ratio = len(glob.glob(train_data_dir+"/0/*.jpeg", recursive=True)) / len(glob.glob(train_data_dir+"/1/*.jpeg", recursive=True))
total_train = len(glob.glob(train_data_dir+"/**/*.jpeg", recursive=True))
total_val = len(glob.glob(validation_data_dir+"/**/*.jpeg", recursive=True))
total_test = len(glob.glob(test_data_dir+"/**/*.jpeg", recursive=True))
NB_EPOCH = 1000 
WEIGHTS_FN = "weights_third/{epoch:02d}-{val_loss:.2f}.hdf5"

def conv(num_filters, filter_size, **kwargs):
	return Convolution2D(num_filters, filter_size, filter_size, border_mode="same", W_regularizer=reg, **kwargs)
def mp(dim, stride):
	return MaxPooling2D(pool_size=(dim, dim), strides=(stride, stride))
def ninety(mat):
	T.set_subtensor(mat[0], K.transpose(mat[0][::-1]))
	return mat
def one_eighty(mat):
	return mat[:, ::-1]
def neg_ninety(mat):
	T.set_subtensor(mat[0], K.transpose(mat[0])[::-1])
	return mat

inp = Input((1, IMG_ROWS, IMG_COLS))
convs = conv(16,3)(inp)
convs = LeakyReLU(ALPHA)(convs)
convs = conv(16,1)(convs)
convs = LeakyReLU(ALPHA)(convs)
convs = mp(3,2)(convs)

convs = conv(32,3)(convs)
convs = LeakyReLU(ALPHA)(convs)
convs = conv(32,1)(convs)
convs = LeakyReLU(ALPHA)(convs)
convs = mp(3,2)(convs)

convs = conv(64,3)(convs)
convs = LeakyReLU(ALPHA)(convs)
convs = conv(64,1)(convs)
convs = LeakyReLU(ALPHA)(convs)
convs = mp(3,2)(convs)

convs = conv(128,3)(convs)
convs = LeakyReLU(ALPHA)(convs)
convs = conv(128,1)(convs)
convs = LeakyReLU(ALPHA)(convs)
convs = mp(3,2)(convs)

convs = conv(256,3)(convs)
convs = LeakyReLU(ALPHA)(convs)
convs = conv(256,1)(convs)
convs = LeakyReLU(ALPHA)(convs)
convs = mp(3,2)(convs)

convs = conv(384,3)(convs)
convs = LeakyReLU(ALPHA)(convs)
convs = conv(384,1)(convs)
convs = LeakyReLU(ALPHA)(convs)
convs = mp(3,2)(convs)

convs = conv(512,3)(convs)
convs = LeakyReLU(ALPHA)(convs)
convs = conv(512,3)(convs)
convs = LeakyReLU(ALPHA)(convs)
convs = mp(3,2)(convs)

convs = conv(512,3)(convs)
convs = LeakyReLU(ALPHA)(convs)
convs = conv(512,3)(convs)
convs = LeakyReLU(ALPHA)(convs)
convs = mp(3,2)(convs)

convs = Dropout(.5)(convs)
convs = Flatten()(convs)
internal = Model(inp, convs)

def run_rotations(img):
	b, c, d = get_rot(img[0])
	return (img, b, c, d)
	
def clip(inp):
	return K.clip(inp, 0, 1)
	
def accuracy(y_true, y_pred):
	return K.mean(K.equal(K.round(y_pred), y_true))

def normalize(img):
	img -= K.mean(img)
	img /= K.std(img)
	return img

def f1_score(y_true, y_pred):
	y_pred = K.round(y_pred)
	tp = K.sum(K.equal(y_pred, 1) * K.equal(y_true, 1))
	fp = K.sum(K.equal(y_pred, 1) * K.equal(y_true, 0))
	fn = K.sum(K.equal(y_pred, 0) * K.equal(y_true, 1))
	precision = K.switch(K.equal(tp, 0), 0, tp/(tp+fp))
	recall = K.switch(K.equal(tp, 0), 0, tp/(tp+fn))
	return K.switch(K.equal(precision+recall, 0), 0, 2*(precision*recall)/(precision+recall))
def scaled_binary_crossentropy(y_true, y_pred):
	cost = binary_crossentropy(y_true, y_pred)
	mult = y_true*(af_ratio*294.500038) + 1
	return cost * mult
	
inp2 = Input((1, IMG_ROWS, IMG_COLS))
model = Lambda(normalize, input_shape=(1, IMG_ROWS, IMG_COLS), output_shape=(1, IMG_ROWS, IMG_COLS))(inp2)
b = Lambda(ninety)(model)
c = Lambda(one_eighty)(model)
d = Lambda(neg_ninety)(model)
a = internal(model)
b = internal(b)
c = internal(c)
d = internal(d)
model = merge([a,b,c,d], mode="ave")
model = Dense(1024, W_regularizer=reg)(model)
model = LeakyReLU(ALPHA)(model)
model = Dense(1024, W_regularizer=reg)(model)
model = LeakyReLU(ALPHA)(model)
model = Dense(1, W_regularizer=reg)(model)
model = Activation("sigmoid")(model)
#model = LeakyReLU(ALPHA)(model)
#model = Lambda(clip, output_shape=lambda x:x)(model)
model = Model(inp2, model)
model.summary()
print("AF RATIO",af_ratio)
model.compile(loss="binary_crossentropy", 
			optimizer=SGD(lr=LR, momentum=.9, nesterov=True),
			metrics=[accuracy, f1_score])
model.load_weights("weights_third/Aft1.hdf5")

train_datagen = ImageDataGenerator(
		)
		
test_datagen = ImageDataGenerator(
		)

train_generator = train_datagen.flow_from_directory(
		train_data_dir,
		target_size=(IMG_ROWS, IMG_COLS),
		batch_size=batch_size,
		class_mode="sparse",
		color_mode="grayscale",
		shuffle=True,
	   )
		
validation_generator = test_datagen.flow_from_directory(
		validation_data_dir,
		target_size=(IMG_ROWS, IMG_COLS),
		batch_size=batch_size,
		class_mode="sparse",
		color_mode="grayscale",
	   )

test_generator = test_datagen.flow_from_directory(
		test_data_dir,
		target_size=(IMG_ROWS, IMG_COLS),
		batch_size=batch_size,
		class_mode="sparse",
		color_mode="grayscale",
	 )
try:
	history = History()
	model.fit_generator(
		train_generator,
		samples_per_epoch=total_train,
		nb_epoch=NB_EPOCH,
		validation_data=validation_generator,
		nb_val_samples=total_val,
		callbacks=[
			ModelCheckpoint(WEIGHTS_FN),
			history,
			]
		)
		
except KeyboardInterrupt:
	pass
print(history.history)
f = open("plots_third/history.txt", "w")
f.write(str(history.history))
f.close()
model.save_weights(WEIGHTS_FN)
try:
	val_loss, = plt.plot(history.history["val_loss"], "r-", label="Validation Loss")
	loss, = plt.plot(history.history["loss"], "b-", label="Training Loss")
	plt.title("Loss scores (scaled binary crossentropy)")
	plt.legend(handles=[val_loss, loss])
	plt.savefig("plots_third/loss_fig.png")
	plt.close()
	val_f1, = plt.plot(history.history["val_f1_score"], "r-", label="Validation F1")
	f1, = plt.plot(history.history["f1_score"], "b-", label="Test F1")
	plt.title("F1 scores")
	plt.legend(handles=[val_f1, f1])
	plt.savefig("plots_third/f1_fig.png")
	plt.close()
except:
	pass
scores = model.evaluate_generator(test_generator, val_samples=total_test)
print(scores)
