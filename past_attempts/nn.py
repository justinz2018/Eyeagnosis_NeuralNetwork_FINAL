import numpy as np
np.random.seed(37045)


from PIL import Image
from ml_metrics.quadratic_weighted_kappa import quadratic_weighted_kappa

from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import EarlyStopping
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D, AveragePooling2D
from keras.layers.advanced_activations import LeakyReLU
from keras.utils import np_utils
from keras import backend as K

from shuffle_data import shuffle_data

nb_classes = 2
nb_epoch = 100

img_rows, img_cols = 256, 256
nb_filters = 32
nb_pool = 2

nb_validation_samples = 5000
nb_test_samples = 6000

train_data_dir = "train"
validation_data_dir = "validation"
test_data_dir = "test"

model = Sequential()

model.add(Convolution2D(nb_filters, 5, 5, border_mode='same', input_shape=(3, img_rows, img_cols)))
model.add(LeakyReLU(alpha=.01))
model.add(Convolution2D(nb_filters, 3, 3, border_mode='same'))
model.add(LeakyReLU(alpha=.01))

model.add(MaxPooling2D(pool_size=(nb_pool, nb_pool)))
model.add(Dropout(0.25))

model.add(Convolution2D(nb_filters, 5, 5, border_mode='same'))
model.add(LeakyReLU(alpha=.01))
model.add(Convolution2D(nb_filters, 3, 3, border_mode='same'))
model.add(LeakyReLU(alpha=.01))

model.add(MaxPooling2D(pool_size=(nb_pool, nb_pool)))
model.add(Dropout(0.25))

#model.add(MaxPooling2D(pool_size=(nb_pool, nb_pool)))

model.add(Flatten())
model.add(Dense(256))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(1))
model.add(Activation("sigmoid"))
#model.add(Dense(nb_classes))
#model.add(Activation('softmax'))

model.summary()

vals = shuffle_data(nb_validation_samples, nb_test_samples, RNG_seed=np.random.randint(1000000000000), balance=True)

nb_train_samples = 0
for i in vals["train"]:
	nb_train_samples += vals["train"][i]

ratios = {}
for i in vals["train"]:
	if i not in ratios:
		ratios[i] = 0
	ratios[i] += vals["train"][i]
for i in vals["test"]:
	if i not in ratios:
		ratios[i] = 0
	ratios[i] += vals["test"][i]
for i in vals["validation"]:
	if i not in ratios:
		ratios[i] = 0
	ratios[i] += vals["validation"][i]
max_num = -2e9
for i in ratios:
	max_num = max(max_num, ratios[i])
assert(max_num > 0)
for i in ratios:
	ratios[i] = max_num/ratios[i]


import theano
import theano.tensor as T

def scaled_binary_crossentropy(y_true, y_pred):
#	return T.printing.Print("test")(y_true)
	return K.mean(K.binary_crossentropy(y_pred, y_true, from_logits=False)*(35*y_true+1), axis=-1)

model.compile(loss="binary_crossentropy",
              optimizer='adadelta',
              metrics=["accuracy"])

train_datagen = ImageDataGenerator(
#        samplewise_std_normalization=True,
#        samplewise_center=True,
#        shear_range=0.3,
 #       color_shift_range=234.0
#        rotation_range=180,
        horizontal_flip=True,
        vertical_flip=True,
        rescale=1/255)
        
test_datagen = ImageDataGenerator(
#        samplewise_std_normalization=True,
#        samplewise_center=True,
        rescale=1/255)

train_generator = train_datagen.flow_from_directory(
        train_data_dir,
        target_size=(img_rows, img_cols),
        batch_size=16,
        class_mode="binary",
    #    color_mode="grayscale"
       )
        
validation_generator = test_datagen.flow_from_directory(
        validation_data_dir,
        target_size=(img_rows, img_cols),
        batch_size=32,
        class_mode="binary",
    #    color_mode="grayscale"
       )

test_generator = test_datagen.flow_from_directory(
        test_data_dir,
        target_size=(img_rows, img_cols),
        batch_size=32,
        class_mode="binary",
    #    color_mode="grayscale"
     )

model.fit_generator(
        train_generator,
        samples_per_epoch=nb_train_samples,
        nb_epoch=nb_epoch,
        validation_data=validation_generator,
        nb_val_samples=nb_validation_samples,
        callbacks=[EarlyStopping(patience=0)])
'''
score = model.evaluate_generator(
            test_generator,
            val_samples=nb_test_samples)

'''
pred = []
actual = []
for X,Y in test_generator:
	Y_pred = model.predict(X, batch_size=len(X)).tolist()
	for pr in Y_pred:
		pred.append(1.0 if pr[0] > .5 else 0.0)
	actual += Y.tolist()
	if len(pred) >= nb_test_samples:
		break
assert(len(pred) == len(actual))
same = 0
for i in range(len(pred)):
	if abs(pred[i]-actual[i]) <= .000005:
		same += 1
#print("Test score: {}".format(score[0]))
print("Test accuracy: {}".format(same/len(pred)))
numone = 0
for i in actual:
	if i >= .99999:
		numone += 1
pred_numone = 0
for i in pred:
	if i >= .99999:
		pred_numone += 1
print("numone: ",numone)
print("pred_numone: ",pred_numone)
print(pred[:20])
print(actual[:20])

print("KAPPA: ",quadratic_weighted_kappa(pred, actual))

