import cv2
import glob, os
import numpy as np

def process(NAME, DIM=None):
    img = cv2.imread(NAME)
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    _,thresh = cv2.threshold(gray,1,255,cv2.THRESH_BINARY)
    # contours,hierarchy = cv2.findContours(thresh,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
    _,contours,hierarchy = cv2.findContours(thresh,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
    maxA = -1
    ind = -1
    for i in range(len(contours)):
        x = cv2.contourArea(contours[i])
        if x > maxA:
             maxA = x
             ind = i

    cnt = contours[ind]
    x,y,w,h = cv2.boundingRect(cnt)
    crop = img[y:y+h,x:x+w]
    if DIM is None:
            DIM = np.min(crop.shape[:2])
            DIM = (DIM, DIM)
    print(DIM)
    resized = cv2.resize(crop, DIM)
    cv2.imwrite(NAME,resized)
if __name__ == "__main__":
    count = 0
    for infile in glob.glob("*.jpeg", recursive=False):
        process(infile)
        count += 1
        print(count)
