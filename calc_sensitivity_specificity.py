def calc(THRESHOLD, y_pred, y_true):
	tp = 0
	p = 0
	tn = 0
	n = 0
	for i, val in enumerate([t >= THRESHOLD for t in y_pred]):
		if val: p += 1
		if val and y_true[i] >= .9999: tp += 1
		if not val: n += 1
		if not val and y_true[i] <= .0001: tn += 1
	return (tp, p, tn, n)	# sensitivity: tp/p and specificity: tn/n

def get_pred_true_fn():
	a = eval(open("final_eval_pred.txt","r").read())
	b = []
	for i in a:
		b.append(i[0])
	c = eval(open("final_eval_true.txt","r").read())
	# b, c = pred, true
	d = eval(open("fileclasses.txt").read())
	e = open("trainLabels.csv").read().split('\n')[1:]
	f = {}
	for i in e:
		if len(i.split(',')) < 2: continue
		g, h = i.split(',')
		f[g] = int(h)
	j = []
	for i in d:
		j.append(i[i.find('/')+1:i.find('.')])
	k = []
	for i in j:
		k.append(f[i])
	# k = classes
	return (b,c,k)
