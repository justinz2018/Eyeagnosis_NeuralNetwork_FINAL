from __future__ import print_function
import random, glob, sys, json, pickle

import keras
import keras.backend as K
from keras.preprocessing import image
from convnets import *
import PIL
from PIL import Image
import matplotlib.pyplot as plt
import numpy as np
import theano.tensor as T


import keras
from keras.layers import merge
from keras.regularizers import l2
from sklearn.utils.class_weight import compute_class_weight
from convnets import *
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.convolutional import Convolution2D, MaxPooling2D, ZeroPadding2D, AveragePooling2D
from keras.layers.core import Dense, Activation, Flatten, Dropout, Lambda
from keras.layers.normalization import BatchNormalization
from keras.models import Model
from keras.layers import Input
from keras.optimizers import Adam, SGD
from keras.preprocessing.image import load_img, img_to_array
import keras.backend as K
from keras.metrics import binary_crossentropy
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import EarlyStopping, ModelCheckpoint, History, LearningRateScheduler
import numpy as np
import matplotlib.pyplot as plt
import theano.tensor as T


def clip(inp):
	return K.clip(inp, 0, 1)
	
def normalize(img):
	img -= K.mean(img)
	img /= K.std(img)
	return img
def ninety(mat):
	T.set_subtensor(mat[0], K.transpose(mat[0][::-1]))
	T.set_subtensor(mat[1], K.transpose(mat[1][::-1]))
	T.set_subtensor(mat[2], K.transpose(mat[2][::-1]))
	return mat
def one_eighty(mat):
	return ninety(ninety(mat))
def neg_ninety(mat):
	T.set_subtensor(mat[0], K.transpose(mat[0])[::-1])
	T.set_subtensor(mat[1], K.transpose(mat[1])[::-1])
	T.set_subtensor(mat[2], K.transpose(mat[2])[::-1])
	return mat

def f1_score(y_true, y_pred):
	y_pred = K.round(y_pred)
	tp = K.sum(K.equal(y_pred, 1) * K.equal(y_true, 1))
	fp = K.sum(K.equal(y_pred, 1) * K.equal(y_true, 0))
	fn = K.sum(K.equal(y_pred, 0) * K.equal(y_true, 1))
	precision = K.switch(K.equal(tp, 0), 0, tp/(tp+fp))
	recall = K.switch(K.equal(tp, 0), 0, tp/(tp+fn))
	return K.switch(K.equal(precision+recall, 0), 0, 2*(precision*recall)/(precision+recall))



NAME = "resnet_768"
RNG_SEED = 123454321
DROPOUT = 0
L2_REG = 0
ALPHA = .01
WEIGHTS_FN = "weights_"+NAME+"/{epoch:03d}-{val_loss:.2f}.hdf5"



sys.setrecursionlimit(10000)
random.seed(RNG_SEED)
np.random.seed(random.randint(0, 4294967295))

def f1_score(y_true, y_pred):
	y_pred = K.round(y_pred)
	tp = K.sum(K.equal(y_pred, 1) * K.equal(y_true, 1))
	fp = K.sum(K.equal(y_pred, 1) * K.equal(y_true, 0))
	fn = K.sum(K.equal(y_pred, 0) * K.equal(y_true, 1))
	precision = K.switch(K.equal(tp, 0), 0, tp/(tp+fp))
	recall = K.switch(K.equal(tp, 0), 0, tp/(tp+fn))
	return K.switch(K.equal(precision+recall, 0), 0, 2*(precision*recall)/(precision+recall))

IMG_NAME = "test/1/35804_left.jpeg"
INPUT_DIM = 1024
EPOCH = 141

IMG_ROWS = IMG_COLS = 512

def get_network():
	internal = ResNet_50()
	inp2 = Input((3, IMG_ROWS, IMG_COLS))
	model = Lambda(normalize)(inp2)
	b = Lambda(ninety)(model)
	c = Lambda(one_eighty)(model)
	d = Lambda(neg_ninety)(model)
	a = internal(model)
	b = internal(b)
	c = internal(c)
	d = internal(d)
	if DROPOUT is not None:
		a = Dropout(DROPOUT)(a)
		b = Dropout(DROPOUT)(b)
		c = Dropout(DROPOUT)(c)
		d = Dropout(DROPOUT)(d)
	model = merge([a,b,c,d], mode="ave")
	model = Dense(1024)(model)
	model = LeakyReLU(ALPHA)(model)
	
	if DROPOUT is not None:
		model = Dropout(DROPOUT)(model)
	
	model = Dense(512)(model)
	model = LeakyReLU(ALPHA)(model)
	model = Dense(1)(model)
	model = Activation("sigmoid")(model)
	#model = LeakyReLU(ALPHA)(model)
	#model = Lambda(clip, output_shape=lambda x:x)(model)
	model = Model(inp2, model)
	if L2_REG is not None:
		print("L2: {}".format(L2_REG))
		for layer in model.layers:
			layer.W_regularizer = l2(L2_REG)

	model.summary()
	
	weights = glob.glob(WEIGHTS_FN.format(epoch=EPOCH, val_loss=999.99).replace("999.99","*"))[0]
	print("LD WEIGHTS: {}".format(weights))
	model.load_weights(weights)
	opt = pickle.load(open("weights_"+NAME+"/{:03d}_opt.p".format(EPOCH), "rb"))
	
	model.compile(loss="binary_crossentropy",
		#	optimizer=SGD(lr=LR, momentum=.9, nesterov=True),
			optimizer=opt,
			metrics=["accuracy", f1_score])
	return model		

im = image.img_to_array(image.load_img(IMG_NAME, target_size=(INPUT_DIM, INPUT_DIM)))
print(im.shape)
model = get_network()
model_input_dim = model.layers[0].input_shape[2]
assert(model.layers[0].input_shape[2] == model.layers[0].input_shape[3])
heatmap_dim = INPUT_DIM-model_input_dim+1
print("Heatmap dimension: {}".format(heatmap_dim))
out = []
tot = 0
for i in range(heatmap_dim):
	tmp = []
	for j in range(heatmap_dim):
		inp = im[:,i:i+model_input_dim,j:j+model_input_dim]
		tmp.append(inp)
	print(np.array(tmp).shape)
	outs = model.predict(np.array(tmp), batch_size=1, verbose=1)
	tot += len(outs)
	out.append(outs)
	print("{}/{}".format(tot, heatmap_dim**2))
	print(outs)
pickle.dump(out, open("heatmap_outs.p", "wb"))
out = np.squeeze(np.array(out))
print(out.shape)
assert(out.shape == (heatmap_dim, heatmap_dim))
plt.imshow(out, interpolation="nearest", cmap="winter")
plt.show()
